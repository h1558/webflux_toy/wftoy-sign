package com.hans.wftoysign;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Webflux toy : SIGN", version = "0.0", description = "Created 2021/11/25"))
public class WftoySignApplication {

    public static void main(String[] args) {
        SpringApplication.run(WftoySignApplication.class, args);
    }

}
